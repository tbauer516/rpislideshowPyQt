import sys
from rpislideshow.window import Window
from config import config

if __name__ == '__main__':
    sys.exit(Window(config).start())
