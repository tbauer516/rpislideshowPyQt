import sys
from PySide2 import QtWidgets, QtCore, QtGui
from rpislideshow.weather.current import Current
from rpislideshow.weather.forecast import ForecastList
from rpislideshow.network.dark_sky import DarkSky
from rpislideshow.network.google_drive import GoogleDrive


class Window:

    app = None
    window = None
    screen_size = None

    current = None
    forecast_list = None
    debug = True

    def __init__(self, config):
        self.app = QtWidgets.QApplication(sys.argv)
        self.app.setStyleSheet(open("rpislideshow/style.qss", "r").read())
        self.screen_size = QtWidgets.QDesktopWidget().screenGeometry(-1)
        self.window = QtWidgets.QWidget()
        self.window.setWindowTitle('rpislideshow')
        self.window.setObjectName('window')

        GoogleDrive(config.GOOGLE_CREDENTIALS)

        self.weather = DarkSky(config.DARKSKY_TOKEN, config.LOCATION)

        self.current = Current(self.weather)
        self.current.update()

        self.forecast_list = ForecastList(self.weather)
        self.forecast_list.update()

        weather_col_wrapper = self.setup_weather_col(
            self.current,
            self.forecast_list.get_list())
        pic_wrapper = self.setup_pic_col()

        win_layout = QtWidgets.QHBoxLayout()
        win_layout.setSpacing(0)
        win_layout.addWidget(pic_wrapper)
        win_layout.addWidget(weather_col_wrapper)

        self.window.setLayout(win_layout)

    def start(self):
        if (self.debug):
            self.window.setGeometry(0, 0, 1280, 720)
            self.window.show()
        else:
            self.window.setGeometry(
                0, 0,
                self.screen_size.width(),
                self.screen_size.height())
            self.window.showFullScreen()

        sys.exit(self.app.exec_())

    def setup_weather_col(self, current, list_of_days):
        weather_col_wrapper = QtWidgets.QFrame()
        weather_col_layout = QtWidgets.QVBoxLayout()
        weather_col_layout.setSpacing(0)
        weather_col_layout.setContentsMargins(0, 0, 0, 0)
        weather_col_wrapper.setLayout(weather_col_layout)
        weather_size_policy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Preferred,
            QtWidgets.QSizePolicy.Preferred)
        weather_size_policy.setHorizontalStretch(1)
        weather_col_wrapper.setSizePolicy(weather_size_policy)

        weather_col_layout.addWidget(current.wrapper)

        for day in list_of_days:
            weather_col_layout.addWidget(day.wrapper)

        return weather_col_wrapper

    def setup_pic_col(self):
        pic_pixmap = QtGui.QPixmap()
        pic_pixmap.load('assets/images/0pZiNmy.jpg')
        pic_pixmap_scaled = pic_pixmap.scaled(
            100, 100,
            QtCore.Qt.KeepAspectRatio)
        pic_wrapper = QtWidgets.QLabel()
        pic_wrapper.setPixmap(pic_pixmap_scaled)
        pic_wrapper.setAlignment(QtCore.Qt.AlignCenter)
        pic_size_policy = QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Preferred,
            QtWidgets.QSizePolicy.Preferred)
        pic_size_policy.setHorizontalStretch(3)
        pic_wrapper.setSizePolicy(pic_size_policy)
        return pic_wrapper
