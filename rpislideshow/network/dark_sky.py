import requests


class DarkSky:

    def __init__(self, token, location):
        self.token = token
        self.location = location

    def get_weather(self, req_type):
        base_url = 'https://api.darksky.net/forecast/'
        exclude = '?exclude=minutely,hourly,alerts,flags'
        to_exclude = {
            'current': 'daily',
            'forecast': 'currently'
        }
        key = self.token
        lat = self.location['lat']
        lng = self.location['lng']

        path = (base_url + key + '/' + str(lat) + ','
                + str(lng) + exclude + ',' + to_exclude[req_type])

        response = requests.get(path)
        return response.json()
