from datetime import datetime
from threading import Thread
import time
from PySide2.QtWidgets import QLabel

class Clock:

    time_format = '%I:%M %p'

    def __init__(self):
        self.widget = QLabel()
        # clock_thread = Thread(target=self.loop, name='Thread-Clock')
        # clock_thread.start()

    def loop(self):
        while True:
            self.update()
            time.sleep(10)

    def update(self):
        curr = datetime.now().strftime(self.time_format)
        self.widget.setText(curr)