from PySide2.QtWidgets import QGridLayout, QLabel, QFrame
from PySide2.QtSvg import QSvgWidget
from ..clock.clock import Clock

class Current:

    def __init__(self, weather_request):
        self.layout = QGridLayout()
        self.wrapper = QFrame()
        self.wrapper.setLayout(self.layout)
        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0, 0, 0, 0)

        self.time = Clock()
        self.icon = QSvgWidget()
        self.temp = QLabel()
        self.precip = QLabel()
        self.wind = QLabel()

        self.layout.addWidget(self.time.widget, 0, 0, 1, 2)
        self.layout.addWidget(self.icon, 1, 0, 3, 1)
        self.layout.addWidget(self.temp, 1, 1, 1, 1)
        self.layout.addWidget(self.wind, 2, 1, 1, 1)
        self.layout.addWidget(self.precip, 3, 1, 1, 1)

        self.weather_request = weather_request

    def update(self):
        response = self.weather_request.get_weather('current')
        data = response['currently']
        self.icon.load('assets/weather/' + data['icon'] + '.svg')
        self.time.update()
        self.temp.setText(str(data['temperature']) + ' °F')
        self.wind.setText(str(data['windSpeed']) + ' MPH')
        self.precip.setText(str(data['precipProbability']) + ' %')
