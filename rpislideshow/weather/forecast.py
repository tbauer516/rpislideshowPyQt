from PySide2.QtWidgets import QGridLayout, QLabel, QFrame
from PySide2.QtSvg import QSvgWidget

class ForecastList:

    def __init__(self, weather_request):
        self.forecasts = [Forecast() for i in range(6)]
        self.weather_request = weather_request

    def get_list(self):
        return self.forecasts

    def update(self):
        response = self.weather_request.get_weather('forecast')
        data = response['daily']['data']
        for i in range(len(self.forecasts)):
            self.forecasts[i].update(data[i])

class Forecast:

    def __init__(self):
        self.layout = QGridLayout()
        self.wrapper = QFrame()
        self.wrapper.setLayout(self.layout)
        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0, 0, 0, 0)

        self.icon = QSvgWidget()
        self.day = QLabel()
        self.min_temp = QLabel()
        self.max_temp = QLabel()
        self.precip = QLabel()
        self.wind = QLabel()

        self.layout.addWidget(self.icon, 0, 0, 1, 1)
        self.layout.addWidget(self.day, 1, 0, 1, 1)
        self.layout.addWidget(self.min_temp, 1, 1, 1, 1)
        self.layout.addWidget(self.max_temp, 0, 1, 1, 1)
        self.layout.addWidget(self.wind, 0, 2, 1, 1)
        self.layout.addWidget(self.precip, 1, 2, 1, 1)

    def update(self, values):
        self.icon.load('assets/weather/' + values['icon'] + '.svg')
        self.day.setText('Mon')
        self.min_temp.setText(str(values['temperatureLow']) + ' °F')
        self.max_temp.setText(str(values['temperatureHigh']) + ' °F')
        self.precip.setText(str(values['precipProbability']) + ' %')
        self.wind.setText(str(values['windSpeed']) + ' MPH')
